FuLa
====

You like the clean output of a Latex file but you're tired of Latex syntax ?
Then FuLa is for you. Use a markdown-like simple language to wrap your tex
document, set in a config file your usual packages and stuff and (almost) forget
about curly brackets and backslashes.

The documentation is on my [website](http://shgck.io/dev/fula)
