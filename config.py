""" Configuration file module. By defualt, loads config.json """

import os
import json


def new_config():
    return {
        "documentclass": "article",
        "encoding": "utf8x",
        "author": os.getlogin(),
        "title": "Undefined title (use !!title)",
        "autocompile": "pdflatex",
        "autoclean": ["aux", "log"],
        "packages": {}
    }


def config_paths():
    """ Returns the path for the user config file.
        There aren't system wide config files atm, not sure it's useful. """
    if os.name == "nt":
        conf_file = os.path.join(os.environ["APPDATA"], "fula", "config.json")
    elif os.name == "posix":
        conf_file = os.path.join(os.path.expanduser("~"),
                                 ".config", "fula", "config.json")
    # TODO : what happens on OS X ?
    else:
        conf_file = os.path.join(os.path.expanduser("~"),
                                 ".config", "fula", "config.json")
    return conf_file


def load_config():
    config = new_config()
    user_conf = config_paths()

    try:
        with open(user_conf, "r") as config_file:
            config.update(json.load(config_file))
    except IOError:
        print("Cannot open the config file at", user_conf)

    return config

