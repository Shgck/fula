""" FuLa file parser. A class seems more adapted to keep track of the global
state of the file.

It actually does more than just parsing, it creates intermediate Tex directives
as I didn't feel like creating an intermediate representation of the document. """

import re
from latex import tex_cmd


class FulaParser(object):
    """
    Attributes :
        content : list of strings, parsed content
        config : global config
        begin_stack : stack of the environment we entered in, so we can keep track of
            the environment we're in to reuse them at the next !!e
        manual_begin_document : if True, the writer won't have to generate
            a begin{document} as it's already in the parsed content.
        list_depth : number of itemized environment we're in
        preformatted_zone : if True, the parser doesn't try to parse commands through the text.

    """

    re_verbatim = re.compile(r"(~~\S(?:.*?\S)??~~)")
    re_emph = re.compile(r"(//\S(?:.*?\S)??//)")

    def __init__(self):
        self.content = []
        self.config = None
        self.begin_stack = []
        self.list_depth = 0
        self.preformatted_zone = False

        # Here comes the flags for specific package or command auto-input
        self.manual_begin_document = False
        self.using_images = False


    def parse_file(self, path, config):
        """ Parses Fula file at path. Returns True on success. """
        self.config = config
        self._handle_config()
        try:
            with open(path, "rt") as input_file:
                self._get_elements(input_file)
        except IOError:
            return False

        if self.using_images:
            config["packages"]["graphicx"] = ""

        return True


    def _get_elements(self, input_file):
        """ Read every line of the file, looking for commands to handle,
            or simply add  """
        for line in input_file:
            lstripped = line.lstrip()

            # When parsing preformatted content, just look for the next !!e
            if self.preformatted_zone:
                if lstripped.startswith("!!e"):
                    self._end()
                else:
                    self._add_line(line)
                continue

            # Handle commands
            if lstripped.startswith("!!"):
                self._handle_command(lstripped[2:])

            # Handle titles
            elif line.startswith("#"):
                self._handle_titles(lstripped)

            # Handle list items when in list mode
            elif self.list_depth > 0 and lstripped.startswith("*"):
                if self.begin_stack[-1] in ("itemize", "enumerate"):
                    self._add_line("\\item " + lstripped.lstrip("* "))
                elif self.begin_stack[-1] == "description":
                    lstripped = lstripped.lstrip("* ")
                    if ":" in lstripped:
                        desc, content = lstripped.split(":", 1)
                        self._add_line("\\item [{}] {}\n".format(desc.strip(), content.strip()))
                    else:
                        self._add_line("\\item [{}]\n".format(lstripped.strip()))

            # Else it can be text or Latex, just appends it to the content.
            else:
                self._add_line(lstripped)


    def _add_line(self, line):
        """ Adds a line to the content. If it's rich, its content is parsed and translated. """
        self.content.append(self._inline_commands(line))


    def _add_tex_command(self, name, args = None, options = None, disable_args = False):
        """ Add a command formatted "<backslash>name[options]{args}". See latex.tex_cmd. """
        self.content.append(tex_cmd(name, args, options, disable_args) + "\n")


    def _begin(self, args, append_crap = None):
        """ Begins new environment.

        The append_crap adds stuff after the environment begin, usually options
        that are set after the main command arg ( beign{env}[opt] ). Fuck this syntax.
        """
        self.begin_stack.append(args)
        if not append_crap:
            self._add_tex_command("begin", args)
        else:
            self.content.append(tex_cmd("begin", args) + append_crap + "\n")

        if args == "itemize" or args == "description" or args == "enumerate":
            self.list_depth += 1
        if args == "document":
            self.manual_begin_document = True
        if args == "verbatim":
            self.preformatted_zone = True


    def _end(self):
        """ Ends env. """
        args = self.begin_stack.pop()
        self._add_tex_command("end", args)

        if args == "itemize":
            self.list_depth -= 1
        if args == "verbatim":
            self.preformatted_zone = False


    def _handle_config(self):
        """ Reads the config dict to handle some options """
        self.config["packages"]["inputenc"] = self.config["encoding"]


    def _handle_command(self, cmd):
        """ Handles all fula commands parsed in the text file. """
        cmd = cmd.split(None, 1)
        command = cmd[0]
        args = cmd[1].rstrip("\r\n") if len(cmd) > 1 else None

        try:
            # General simple commands
            if command == "dc":
                self.config["documentclass"] = args
            elif command == "title":
                self.config["title"] = args
            elif command == "author":
                self.config["author"] = args

            # Add a package
            elif command == "p":
                pack = args.split(None, 1)
                self.config["packages"].update(
                    { pack[0]: pack[1] if len(pack) > 1 else "" }
                )

            # Begin / End
            # Doesn't handle arguments !
            elif command == "b":
                self._begin(args)
            elif command == "e":
                self._end()

            # Images and figures
            elif command == "img":
                self._handle_images(args)
            elif command == "fig":
                self.using_images = True
                fig_args = args.split(" - ", 1)
                self._begin("figure", append_crap = "[h]")
                self._add_tex_command("center", disable_args = False)
                self._handle_images(fig_args[0])
                if len(fig_args) > 1:
                    self._add_tex_command("caption", fig_args[1])
                self._end()

            # Aliases
            elif command == "doc":
                self._begin("document")
            elif command == "list":
                self._begin("itemize")
            elif command == "desc":
                self._begin("description")
            elif command == "enum":
                self._begin("enumerate")

            elif command == "f":
                if self.config["documentclass"] == "beamer":
                    self._begin("frame")
                    if args:
                        self._add_tex_command("frametitle", args)
            elif command == "pre" or command == "code":
                self._begin("verbatim")

            else:
                print("Command", command, "is undefined.")

        except IndexError:
            print("An error occured, probably wrong command syntax : "
                  "command : {}, args : {}".format(command, args))


    def _handle_images(self, args):
        """ Add an image with the includegraphics command. """
        self.using_images = True
        args = args.split(None, 1)
        self._add_tex_command("includegraphics", args[0], args[1] if len(args) > 1 else None)


    def _handle_titles(self, line):
        """ Handles Fula titles, either #, ## or ###. """
        hashes, title = line.split(" ", 1)
        depth = min(hashes.count("#"), 3)
        title = title.rstrip("\r\n")

        # Generates "section", "section*", "subsection", ... according to params.
        cmd = "{}section{}".format(
            "sub" * (depth-1),
            "*" if "*" in hashes else ""
        )

        self._add_tex_command(cmd, title)

        if depth > 3:
            print("Title with too much hashes (max depth is 3)")


    @staticmethod
    def _inline_commands(line):
        """ Look for inline special syntax like /emphasis/
            and returns the line with the appropriate Latex. """
        def verbatim(m):
            return "\\texttt{{{}}}".format(m.group(0).strip("~"))
        def emph(m):
            return "\\emph{{{}}}".format(m.group(0).strip("/"))

        line = FulaParser.re_verbatim.sub(verbatim, line)
        line = FulaParser.re_emph.sub(emph, line)
        return line
