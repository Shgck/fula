""" Module to write down the .tex file from the parsed elements. """

from latex import tex_cmd


class LatexExporter(object):

    def export(self, parser, path, config):
        """ Exports the content in the file at path, using config. Returns True on success. """
        try:
            with open(path, "wt") as output_file:
                self._write_latex(output_file, parser, config)
        except IOError:
            return False
        return True

    @staticmethod
    def _write_latex(f, parser, config):
        """ Writes the Latex file.
            Uses a side function to directly write Tex commands in the file f. """
        def fw(cmd, sep = "\n"):
            f.write(cmd + sep)

        fw(tex_cmd("documentclass", config["documentclass"]) + "\n")

        for p_name, p_args in config["packages"].items():
            fw(tex_cmd("usepackage", p_name, p_args))

        fw("")

        fw(tex_cmd("title", config["title"]))
        fw(tex_cmd("author", config["author"]))

        fw("")

        if not parser.manual_begin_document:
            fw(tex_cmd("begin", "document"))
            fw(tex_cmd("maketitle", disable_args = True))

        for e in parser.content:
            fw(e, sep = ("" if e else "\n"))

        if not parser.manual_begin_document:
            fw("\n" + tex_cmd("end", "document"))
