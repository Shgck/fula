#!/usr/bin/python3

##########################################################################
# This program is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
# (at your option) any later version.                                    #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                        #
# You should have received a copy of the GNU General Public License      #
# along with this program. If not, see <http://www.gnu.org/licenses/>.   #
##########################################################################

"""
FuLa is a small language to write Latex files while hiding most of ugly Latex syntax.
I did it for my own purpose, but I guess I'm not the only European who is
tired of using Alt Gr every two words.

website : search for FuLa on Bitbucket.

Shgck - 2014
"""

import os
import sys
import subprocess

import fula_parser
import latex_exporter
import config as conf


def main():
    if len(sys.argv) != 2:
        print("Usage : python3 fula file.ful")
        sys.exit(-1)

    print("FuLa - by Shgck")

    config = conf.load_config()

    input_dir, input_file = os.path.split(sys.argv[1])
    if input_dir:
        os.chdir(input_dir)

    # Read the FuLa file
    parser = fula_parser.FulaParser()
    if parser.parse_file(input_file, config):
        print(input_file, "parsed.")
    else:
        print("Cannot open", input_file)
        sys.exit(-1)

    # Write the LaTeX file
    tex_path = os.path.splitext(input_file)[0] + ".tex"
    exporter = latex_exporter.LatexExporter()
    if exporter.export(parser, tex_path, config):
        print("TeX written.")
    else:
        print("Cannot write", tex_path)
        sys.exit(-1)

    # Additionnal operations
    if config["autocompile"] != "" and config["autocompile"] != "off":
        print("Automatically calls", config["autocompile"])
        try:
            subprocess.call([config["autocompile"], tex_path])
            print(config["autocompile"], "ended.")
        except OSError:
            print("Couldn't start", config["autocompile"])

        file_name = os.path.splitext(tex_path)[0]
        for ext in config["autoclean"]:
            to_remove = file_name + "." + ext
            print("Removing", to_remove)
            try:
                os.remove(to_remove)
            except OSError:
                print("Failed to delete", to_remove)


if __name__ == "__main__":
    main()
