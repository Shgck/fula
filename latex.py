""" Module for static Tex related functions. """


def tex_cmd(name, args = None, options = None, disable_args = False):
    """ Returns a strng formatted "<backslash>name[options]{args}".

        Leave args or options to None or "" if the command
        doesn't need them. Set disable_args to True if the command doesn't
        need the {} part. """
    if disable_args:
        args = ""
    else:
        args = "{{{}}}".format(args) if args else "{}"

    options = "[{}]".format(options) if options else ""

    return "\\{}{}{}".format(name, options, args)

